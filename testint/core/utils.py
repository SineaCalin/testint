import requests
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError


class NewsClient(object):
    def __init__(self):
        self.auth_key = settings.API_KEY
        self.url = "http://newsapi.org/v2/everything?"

    def get_everything_from(self, keyword="Apple", from_date=None):
        response = requests.get(
            self.url
            + "q="
            + keyword
            + "&from="
            + str(from_date)
            + "&apiKey="
            + self.auth_key
        )
        if response.status_code == 200:
            return response.json()
        else:
            raise ValidationError(
                detail=_("SYNC_ERROR_GET_EVERYTHING_PLEASE_SPECIFIY_TOPIC")
            )
