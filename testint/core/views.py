# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from testint.core.models import Articles
from testint.core.serializers import ArticleSerializer
from testint.core.utils import NewsClient


class CustomPagination(PageNumberPagination):
    """
    default page size will be 100 but we can incrase it with page_size url param
    """

    page_size = 100
    page_size_query_param = "page_size"
    max_page_size = 1000


class ArticlesViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, GenericViewSet):
    queryset = Articles.objects.all()
    serializer_class = ArticleSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    ordering_fields = ["publishedAt"]
    pagination_class = CustomPagination

    """
    sync_news api call is used for connecting to news api platform and
    retrieving specific news then saving it to db
    you can supply the following params to the url
    :topic used to retrieve news about a specific topic default="Apple"
    :start_date used to retrieve news starting from a specific date
    """

    @action(detail=False, methods=["post"])  # TO-DO Change this to POST
    def sync_news(self, request):
        client = NewsClient()
        topic = request.data.get("topic")
        start_date = request.data.get(
            "start_date"
        )  # not used yet we could add more but its out of scope
        json = client.get_everything_from(topic, start_date)
        serializer = ArticleSerializer(data=json["articles"], many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data)
