from django.db import models

# Create your models here.


class Source(models.Model):
    django_id = models.AutoField(primary_key=True)
    id = models.CharField(max_length=250, null=True, blank=True)
    name = models.CharField(max_length=250, null=True, blank=True)


class Articles(models.Model):
    source = models.ForeignKey(Source, on_delete=models.CASCADE, null=True)
    author = models.CharField(max_length=300, null=True)
    description = models.TextField()
    url = models.URLField(max_length=2048)
    urlToImage = models.URLField(max_length=2048, null=True)
    publishedAt = models.DateTimeField()
    content = models.TextField(null=True)
