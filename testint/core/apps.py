from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = "testint.core"
