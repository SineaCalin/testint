from django.db import transaction
from rest_framework import serializers

from testint.core.models import Articles, Source


class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Source


class ArticleSerializer(serializers.ModelSerializer):
    source = SourceSerializer(many=False)

    @transaction.atomic
    def create(self, validated_data):
        if "source" in validated_data and (
            validated_data["source"]["id"] or validated_data["source"]["name"]
        ):
            source = validated_data.pop("source")
            source_object, _ = Source.objects.get_or_create(source)
            validated_data["source"] = source_object
        instance, _ = Articles.objects.get_or_create(
            url=validated_data["url"], defaults=validated_data
        )
        return instance

    class Meta:
        fields = "__all__"
        model = Articles
