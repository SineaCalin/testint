import pytest
from dateutil.parser import parse
from rest_framework.test import APIClient

from testint.core.models import Articles
from testint.core.tests.factory import ArticleFactory, UserFactory


@pytest.mark.django_db
def test_sync_data_from_api():
    user = UserFactory()
    client_factory = APIClient()
    client_factory.force_login(user)
    response = client_factory.post("/api/news/sync_news/", {"topic": "Apple"})
    assert response.status_code == 200
    assert len(response.data) == Articles.objects.count()


@pytest.mark.django_db
def test_dsplay_most_recent_news():
    user = UserFactory()
    client_factory = APIClient()
    client_factory.force_login(user)
    ArticleFactory.create_batch(30)
    response = client_factory.get("/api/news/?page_size=20&ordering=-publishedAt")
    assert response.status_code == 200
    assert len(response.data["results"]) == 20
    assert parse(response.data["results"][0]["publishedAt"]) > parse(
        response.data["results"][1]["publishedAt"]
    )
    # silly test it can be done a bit better by checking if all values are sorted
