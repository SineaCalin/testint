import datetime

import factory.fuzzy
from django.contrib.auth import get_user_model
from factory.django import DjangoModelFactory
from faker import Factory as FakerFactory
from pytz import UTC

from testint.core.models import Articles

faker = FakerFactory.create()


class UserFactory(DjangoModelFactory):
    username = factory.Faker("email")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    email = factory.Faker("email")

    class Meta:
        model = get_user_model()


class ArticleFactory(DjangoModelFactory):
    publishedAt = factory.fuzzy.FuzzyDateTime(datetime.datetime(2008, 1, 1, tzinfo=UTC))
    description = faker.text(max_nb_chars=200, ext_word_list=None)
    url = faker.url(schemes=None)

    class Meta:
        model = Articles
