Description
===========

Api
~~~

This is an interview test, the project has 2 api routes.
The first route is  ::

    /api/news/news_sync/
    method : POST
    post_params :  {"topic": ""}) #default is Apple

This route will sync specific news depending on the topic

The second route is ::

    /api/news/
    methods: POST, GET
    post_params: { all required model fields are required }





This route will either display specific news or can actually create a single news item like sync

Time Spent
~~~~~~~~~~

The project duration was around 4 hours in total. I did not need to add docker and documentation but
i had some time to spare and i went a bit wild with it.

I also planed to make an nginx version for production but i ran out of time.
