Getting Started
===============

Deployment
~~~~~~~~~~
Before deployment setup the environment file **.django_env** with the following params **DATABASE_URL**
you can aso check local.yml for location and name

To deploy to local use the following command ::

   $ docker-compose -f local.yml up

To access the container we can use the following command ::

   $ docker-compose -f local.yml exec django sh

Setting Up Your Users
~~~~~~~~~~~~~~~~~~~~~

To create an **superuser account**, use this command from either inside or outside the container ::
    $ python manage.py createsuperuser

By default a user is created with the following credentials
username **admint** password **password**

Running it locally without docker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For the purpose of debugging it is recommended to run the project without docker.
The only docker container needed is postgres::

    $ docker-compose -f local.yml up postgres

The second step would be to change your enviromental variables.
You do that by going to the project root and running ::

    $ source export.sh


Only thing left is to get packages and run the server so you can do something like.
We start by creating a virtualenv using::

    $ virtualenv <name_of_env>
    $ source <name_of_env>/bin/activate
    $ pip install -r requirements/local.txt
    $ python manage.py runserver


Debugging
~~~~~~~~~

We have 2 ways of debugging the app depending if we are inside or outside the containter,
We could use ipdb or we can user runserver_plus and debug it on the browser.


