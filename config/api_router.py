from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from testint.core.views import ArticlesViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("news", ArticlesViewSet)

app_name = "api"
urlpatterns = router.urls
