TestInt
=======

Deployment
----------

The following details how to deploy this application.

1.  Create a new file on root called .django_env
2.  Edit the .django_env file and add the following parameters DATABASE_URL=postgres://postgres:postgres@postgres:5432/postgres
    and API_KEY=
3.  docker-compose -f local.yml build
4.  docker-compose -f local.yml up

Further documentation can be found at localhost:8001
